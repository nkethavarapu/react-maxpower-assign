function Footer() {
  var icons = [
    { link: "fa fa-facebook" },

    { link: "fa fa-google-plus" },

    { link: "fa fa-twitter" },

    { link: "fa fa-youtube" },

    { link: "fa fa-instagram" },
  ];

  return (
    <>
      <footer>
        <ul className="social">
          {icons.map((socioimg) => {
            return <Iconsimg img={socioimg.link} />;
          })}
        </ul>
      </footer>
      <footer className="second">
        <p>&copy; MaxPower Design</p>
      </footer>
    </>
  );

  function Iconsimg(props) {
    return (
      <>
        <li>
          <a href="#" target="_blank">
            <i className={props.img}></i>
          </a>
        </li>
      </>
    );
  }
}

export default Footer;
