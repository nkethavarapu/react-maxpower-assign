function Banners() {
  var shortcuts = [
    {
      artid: "tablet",
      id: "tablet2",
      imglink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_ipad_vr0pfu.png",
      text:
        " A clean HTML layout and CSS stylesheet making for a great responsive" +
        " framework to design around that includes a responsive drop down" +
        " navigation menu, image slider, contact form and ‘scroll to the top" +
        " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
        "navigation menu, image slider, contact form and ‘scroll to the top’" +
        " jQuery plugin.",
      name: "Mobile.Tablet.Desktop",
      sectionclass: "inner-wrapper",
    },
    {
      artid: "mobile",
      id: "hand-mobile",
      imglink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571257/hand_mobile_stt5sh.png",
      text:
        " A clean HTML layout and CSS stylesheet making for a great responsive" +
        " framework to design around that includes a responsive drop down" +
        " navigation menu, image slider, contact form and ‘scroll to the top" +
        " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
        "navigation menu, image slider, contact form and ‘scroll to the top’" +
        " jQuery plugin.",
      name: "Across each device",
      sectionclass: "inner-wrapper-2",
    },
    {
      artid: "",
      id: "desktop",
      imglink:
        "https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571256/desktop_fsksnx.png",
      text:
        " A clean HTML layout and CSS stylesheet making for a great responsive" +
        " framework to design around that includes a responsive drop down" +
        " navigation menu, image slider, contact form and ‘scroll to the top" +
        " jQuery plugin.jQuery plugin.that includes a responsive drop down" +
        "navigation menu, image slider, contact form and ‘scroll to the top’" +
        " jQuery plugin.",
      name: "Desktop",
      sectionclass: "inner-wrapper",
    },
  ];
  return (
    <>
      {shortcuts.map((data) => {
        if(data.sectionclass=="inner-wrapper"){
        return (
          <FloatingBanner
            sectionclass={data.sectionclass}
            imglink={data.imglink}
            id={data.id}
            name={data.name}
            text={data.text}
            artid={data.artid}
          />
        );
        }
        else{
            return(
                <FloatingBannerleft
            sectionclass={data.sectionclass}
            imglink={data.imglink}
            id={data.id}
            name={data.name}
            text={data.text}
            artid={data.artid}
          />
            )
        }
      })}
    </>
  );
}
function FloatingBanner(props) {
  return (
    <>
      <section className={props.sectionclass}>
        <article id={props.artid}>
          <img src={props.imglink} />
        </article>
        <aside id={props.id}>
          <h2>{props.name}</h2>
          <p>{props.text}</p>
        </aside>
      </section>
    </>
  );
}
function FloatingBannerleft(props) {
    return (
      <>
        <section className={props.sectionclass}>
          
          <aside id={props.id}>
          <img src={props.imglink} />
            
          </aside>
          <article id={props.artid}>
          <h2>{props.name}</h2>
            <p>{props.text}</p>
          </article>
        </section>
      </>
    );
  }

export default Banners;
