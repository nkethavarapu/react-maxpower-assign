function Header() {
  return (
    <>
      <header>
        <div id="header-inner">
          <a href="#" id="logo"></a>
          <nav>
            <a href="#" id="menu-icon"></a>
            <ul>
              <li>
                <a href="#" className="current">
                  Home
                </a>
              </li>
              <li>
                <a href="#">Skills</a>
              </li>
              <li>
                <a href="#">Portfolio</a>
              </li>
              <li>
                <a href="#">Our team</a>
              </li>
              <li>
                <a href="#">Contact</a>
              </li>
            </ul>
          </nav>
        </div>
      </header>
      <Banner/>
    </>
  );
}
function Banner() {
    return (
      <>
        <section className="banner">
          <div className="banner-inner">
            <img src="https://res.cloudinary.com/dgpmuegqe/image/upload/v1492571172/rocket_design_k4nzbm.png" />
          </div>
        </section>
      </>
    );
  }
export default Header;
