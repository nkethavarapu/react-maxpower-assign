import "./index.css";
import Header from "./header";
import Banners from "./banners";
import Cards from "./cards";
import Footer from "./footer";
import Icons from "./icons";

function Main() {
  return (
    <>
      <Header />
      <Icons />
      <Banners />
      <Cards />
      <Footer />
    </>
  );
}

export default Main;
